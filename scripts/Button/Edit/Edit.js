import Button from "../Button.js";
import {editCard as edit} from "../../functions/indexFunctionReimport.js";
import {TherapistVisitForm, DentistVisitForm, CardioVisitForm} from "../../Form/indexFormReimport.js";

export class Edit extends Button {
    createEditButton() {
        const edit =  this.render()
        this.edit = edit
        edit.addEventListener('click', this.editCard)
        return edit
    }

    editCard = () => {
        const card = this.edit.closest('.card')
        const id = card.id
        const bodyData = card.querySelectorAll('h3, span, p')
        const bodyObj = {
            id
        };
        bodyData.forEach((item) => {
            const text = item.textContent.split(': ').splice(-1,1).join('')
            bodyObj[item.className] = text
        })
        const allData = card.querySelectorAll('h3, span, p, div');
        allData.forEach(item => item.remove())
        switch (bodyObj["card-doctor"]) {
            case 'Терапевт':
                const Therapist = new TherapistVisitForm().renderTherapist(bodyObj)
                card.append(Therapist)
                break;
            case 'Стоматолог':
                const Dentist = new DentistVisitForm().renderDentist(bodyObj)
                card.append(Dentist)
                break;
            case 'Кардиолог':
                const Cardiologist = new CardioVisitForm().renderCardio(bodyObj)
                card.append(Cardiologist)
                break;
        }
    }
}
