import CreateElement from "../../components/CreateElement.js";
import {stateButton} from "../../State/Button/Button.js";
import {Edit} from "../Edit/Edit.js";
import {Delete} from "../Delete/Delete.js";

export class CardButton {
    blockCard =  {
        tag: 'div',
        attr: {
            className: 'card-block-btn',
        }
    }

    render() {

        const cardBlockBtn = new CreateElement(this.blockCard).create()
        const {edit, deleteButton} = stateButton;
        const editBtn = new Edit(edit).createEditButton();
        const deleteBtn = new Delete(deleteButton).createDeleteButton();
        cardBlockBtn.append(editBtn, deleteBtn);
        return cardBlockBtn
    }
}
