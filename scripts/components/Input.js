import CreateElement  from './CreateElement.js'

export class Input extends CreateElement {

    handleFocus = () => {
        if(this.error) {
            // this.error.remove()
        }
        this.elem.classList.remove('error-border')
    };

    handleBlur = () => {
        if(!this.elem.value) {
            // const error = new CreateElement(
            //     {
            //         tag: "p",
            //         attr: {
            //             style: "color: red"
            //         },
            //         content: `${this.errorText}`
            //     }
            //     ).create();
            // this.elem.after(error);
            // this.error = error;
            this.elem.classList.add('error-border')
        }
    };

    render() {
        const {errorText, ...attr} = this.props;
        this.errorText = errorText
        const {attr: attribute} = attr
        const element = this.create();
        if(attribute.required) {
            element.addEventListener("focus", this.handleFocus)
            element.addEventListener("blur", this.handleBlur)
        }
        this.elem = element;
        return element;
    }
}


