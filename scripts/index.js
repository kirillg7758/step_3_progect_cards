import {SearchForm} from './Form/indexFormReimport.js'
import {VisitCardiologist, VisitDentist, VisitTherapist} from './Visited/indexVisitReimport.js'
import {LinkTag} from './State/state.js'
import {Link} from "./Button/Link.js";
import {getCards} from "./functions/getCards.js";
import {createElement} from "./components/component.js";
import {noItems} from "./State/NoItems/NoItems.js";
import {dropId} from './State/dragDropId.js'

const nav = document.querySelector('.navbar');
LinkTag.getContent()
const link = new Link(LinkTag).render()
nav.append(link)

const container = document.getElementById('cards');
const h1 = new createElement(noItems).create();
if (localStorage.getItem('token')) {
    getCards().then((res) => {
        console.log(res.data)
        if (res.data.length) {
            const removeElement = document.getElementById('no-item')
            if (removeElement) {
                removeElement.remove()
            }
            res.data.forEach((item) => {
                const {"select-doctor": doctor} = item

                container.addEventListener('dragover', allowDrop)
                container.addEventListener('drop', drop)

                function allowDrop(e) {
                    e.preventDefault()
                }

                function drop(e) {
                    e.preventDefault()
                    const id = e.dataTransfer.getData("id");
                    const dragElement = document.getElementById(dropId.id);
                    const dropElement = document.getElementById(id)

                    if (e.target.classList.contains('cards')) {
                        dragElement.after(dropElement);
                    }
                }

                switch (doctor) {
                    case 'therapist':
                        const cardTherapist = new VisitTherapist(item).render()
                        container.append(cardTherapist)
                        break;
                    case 'cardiologist':
                        const cardCardiologist = new VisitCardiologist(item).render()
                        container.append(cardCardiologist)
                        break;
                    case 'dentist':
                        const cardDentist = new VisitDentist(item).render()
                        container.append(cardDentist)
                        break;
                }
            })
        } else {
            container.after(h1)
        }
    })
}

nav.after(new SearchForm().renderForm())




