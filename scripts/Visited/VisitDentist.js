import Visit from "./Visit.js";
import CreateElement from "../components/CreateElement.js";
import {stateDentist} from '../State/state.js'
import {CardButton} from "../Button/indexReimportBtn.js";
import {dropId} from "../State/dragDropId.js";
export class VisitDentist extends Visit {
    drag = (e) => {
        e.dataTransfer.setData("id", this.props.id);
        setTimeout(() => {
            console.log(e.target)
            e.target.classList.add('hide')
        }, 0)
    }

    dragEnd = (e) => {
        e.target.classList.remove('hide')
    }

    dragEnter = (e) => {
        if (e.target.id) {
            dropId.id = e.target.id
        }
    }

    render() {
        const res = this.changeState(this.props, stateDentist)

        const {
            titleCard,
            doctorCard,
            blockCard,
            purposeCard,
            lastDayCard,
            priorityCard,
            descriptionCard
        } = res

        const card = this.createCardContainer(this.props.id)
        card.addEventListener('dragstart', this.drag)
        card.addEventListener('dragend', this.dragEnd)
        card.addEventListener('dragenter', this.dragEnter)
        const cardTitle = new CreateElement(titleCard).create()
        const cardDoctor = new CreateElement(doctorCard).create()
        const cardBlock = new CreateElement(blockCard).create()
        const cardPurpose = new CreateElement(purposeCard).create()
        const cardLastDay = new CreateElement(lastDayCard).create()
        const cardPriority = new CreateElement(priorityCard).create()
        const cardDescription = new CreateElement(descriptionCard).create()

        const cardBtn = new CardButton().render()

        cardBlock.append(cardPriority, cardPurpose, cardLastDay)
        card.append(cardTitle, cardDoctor, cardBlock, cardDescription, cardBtn)
        return card
    }

    changeState(props, state) {
        const {
            titleCard,
            priorityCard,
            purposeCard,
            lastDayCard,
            descriptionCard
        } = state;
        const {
            "full-name": name,
            "select-priority": priority,
            target,
            textarea,
            "last-day-visited": lastDay
        } = props

        titleCard.content = titleCard.getContent(name)
        purposeCard.content = purposeCard.getContent(target)
        priorityCard.content = priorityCard.getContent(priority)
        lastDayCard.content = lastDayCard.getContent(lastDay)
        descriptionCard.content = descriptionCard.getContent(textarea)

        return state
    }
}
