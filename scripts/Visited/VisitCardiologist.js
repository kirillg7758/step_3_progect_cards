import Visit from "./Visit.js";
import CreateElement from "../components/CreateElement.js";
import {stateCardio} from '../State/state.js';
import {CardButton} from '../Button/indexReimportBtn.js';
import {dropId} from "../State/dragDropId.js";



export class VisitCardiologist extends Visit{
    drag = (e) => {
        e.dataTransfer.setData("id", this.props.id);
        setTimeout(() => {
            console.log(e.target)
            e.target.classList.add('hide')
        }, 0)
    }

    dragEnd = (e) => {
        e.target.classList.remove('hide')
    }

    dragEnter = (e) => {
        if (e.target.id) {
            dropId.id = e.target.id
        }
    }

    render() {
        const res = this.changeState(this.props, stateCardio)
        const {
            titleCard,
            ageCard,
            blockCard,
            doctorCard,
            purposeCard,
            pressureCard,
            indexWeightCard,
            illnessesCard,
            priorityCard,
            descriptionCard
        } = res

        const card = this.createCardContainer(this.props.id)
        card.addEventListener('dragstart', this.drag)
        card.addEventListener('dragend', this.dragEnd)
        card.addEventListener('dragenter', this.dragEnter)
        const cardTitle = new CreateElement(titleCard).create()
        const cardAge = new CreateElement(ageCard).create()
        const cardBlock = new CreateElement(blockCard).create()
        const cardDoctor = new CreateElement(doctorCard).create()
        const cardPurpose = new CreateElement(purposeCard).create()
        const cardPressure = new CreateElement(pressureCard).create()
        const cardPriority = new CreateElement(priorityCard).create()
        const cardIndexWeight = new CreateElement(indexWeightCard).create()
        const cardIllnesses = new CreateElement(illnessesCard).create()
        const cardDescription = new CreateElement(descriptionCard).create()
        const cardBtn = new CardButton().render()

        cardBlock.append(cardPurpose, cardPriority, cardPressure, cardIndexWeight, cardIllnesses)
        card.append(cardTitle, cardAge, cardDoctor, cardBlock, cardDescription, cardBtn)
        return card
    }

    changeState(props, state) {
        const {
            titleCard,
            ageCard,
            purposeCard,
            pressureCard,
            indexWeightCard,
            illnessesCard,
            priorityCard,
            descriptionCard
        } = state;
        const {
            age,
            "full-name": name,
            "select-priority": priority,
            target,
            pressure,
            "index-weight": weight,
            textarea,
            "diseases-of-cardiovascular-system": illness
        } = props

        titleCard.content = titleCard.getContent(name)
        ageCard.content = ageCard.getContent(age)
        purposeCard.content = purposeCard.getContent(target)

        priorityCard.content = priorityCard.getContent(priority)
        pressureCard.content = pressureCard.getContent(pressure)
        indexWeightCard.content = indexWeightCard.getContent(weight)

        illnessesCard.content = illnessesCard.getContent(illness)
        descriptionCard.content = descriptionCard.getContent(textarea)

        return state
    }
}

