import Modal from "./Modal.js";
import {SelectVisitForm} from "../Form/indexFormReimport.js";

export class ModalAddVisit extends Modal{
    renderModal() {
        const modal = this.render()
        modal.append(new SelectVisitForm().renderForm())
        return modal
    }
}

