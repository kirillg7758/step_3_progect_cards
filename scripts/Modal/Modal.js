import createElement from "../components/CreateElement.js";

export default class Modal {
    constructor(props) {
        this.props = {...props}
    }

    close = (e) => {
        if (e.target.classList.contains('modal')) {
            this.elem.remove();
        }
    }

    modal = {
        tag: 'div',
        attr: {
            className: 'modal',
            id: 'my-modal'
        },
    }

    render() {
        const modal = new createElement(this.modal).create();
        modal.addEventListener('click', this.close)
        this.elem = modal;
        return modal;
    }
}

       

