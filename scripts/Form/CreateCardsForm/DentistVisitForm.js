import {
    Target,
    SelectTagPriority,
    SelectPriority,
    FullName,
    Age,
    TextArea as TextAreaInfo,
    ButtonCreate,
    LastDayVisited
} from '../../State/state.js'
import {SelectVisitForm} from "./SelectVisitForm.js";
import {Input, Select, TextArea, createElement} from "../../components/component.js";
import {editCard} from "../../functions/editCard.js";
import {VisitDentist} from "../../Visited/VisitDentist.js";

export class DentistVisitForm extends SelectVisitForm{
    renderDentist(obj) {
        const div = new createElement({tag: 'div'}).create()
        this.div = div
        const purposeInput = new Input(Target).render()
        const selectPriority = new Select(SelectTagPriority, SelectPriority).render()
        const fioInput = new Input(FullName).render()
        const ageInput = new Input(Age).render()
        const lastVisitDateInput = new Input(LastDayVisited).render()
        const textArea = new TextArea(TextAreaInfo).render()
        const btn = new Input(ButtonCreate).render()

        if (obj) {
            const {
                "card-age": age,
                "card-description": description,
                "card-fio": fio,
                "card-priority": priority,
                "card-purpose": purpose,
                "last-day-visited": day,
                id
            } = obj
            fioInput.value = fio;
            ageInput.value = age;
            purposeInput.value = purpose;
            selectPriority.value = priority;
            textArea.value = description;
            lastVisitDateInput.value = day;
            btn.addEventListener('click', this.putRequest.bind(this, id))
        }
        div.append(
            fioInput,
            ageInput,
            purposeInput,
            selectPriority,
            lastVisitDateInput,
            textArea,
            btn)
        return div
    }

    putRequest(id) {
        const body = this.div.querySelectorAll('input[name], select, textarea');
        const bodyObj = {}
        body.forEach(item => {
            bodyObj[item.name] = item.value
        })
        editCard(id, bodyObj).then((response) => {
            console.log(response.data)
            const div = this.div.closest('.card')
            const prevSibling = div.previousElementSibling
            const nextSibling = div.nextElementSibling
            div.remove()
            const therapist = new VisitDentist(bodyObj).render()
            if (prevSibling) {
                prevSibling.after(therapist)
            } else if(nextSibling) {
                nextSibling.before(therapist)
            } else {
                const container = document.getElementById('cards');
                container.append(therapist)
            }
        })

    }
}

new DentistVisitForm().renderDentist()

