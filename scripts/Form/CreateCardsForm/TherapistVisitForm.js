import {
    Target,
    SelectTagPriority,
    SelectPriority,
    FullName,
    Age,
    TextArea as TextAreaInfo,
    ButtonCreate
} from '../../State/state.js'
import {SelectVisitForm} from "./SelectVisitForm.js";
import {Input, Select, TextArea, createElement} from "../../components/component.js";
import {editCard} from "../../functions/editCard.js";
import {VisitTherapist} from "../../Visited/VisitTherapist.js";


export class TherapistVisitForm extends SelectVisitForm{
    renderTherapist(obj = null) {
        const div = new createElement({tag: 'div'}).create()
        const purposeInput = new Input(Target).render()
        const selectPriority = new Select(SelectTagPriority, SelectPriority).render()
        const fioInput = new Input(FullName).render()
        const ageInput = new Input(Age).render()
        const textArea = new TextArea(TextAreaInfo).render()
        const btn = new Input(ButtonCreate).render()
        this.div = div
        if (obj) {
            const {
                "card-age": age,
                "card-description": description,
                "card-doctor": doctor,
                "card-fio": fio,
                "card-priority": priority,
                "card-purpose": purpose,
                id
            } = obj
            fioInput.value = fio;
            ageInput.value = age;
            purposeInput.value = purpose;
            selectPriority.value = priority;
            textArea.value = description;
            btn.addEventListener('click', this.putRequest.bind(this, id))
        }
        div.append(
            fioInput,
            ageInput,
            purposeInput,
            selectPriority,
            textArea,
            btn)
        return div
    }

    putRequest(id) {
        const body = this.div.querySelectorAll('input[name], select, textarea');
        const bodyObj = {}
        body.forEach(item => {
            bodyObj[item.name] = item.value
            console.log(item.name, item.value)
        })
        console.log(bodyObj)
        editCard(id, bodyObj).then((response) => {
            console.log(response.data)
            const div = this.div.closest('.card')
            const prevSibling = div.previousElementSibling
            const nextSibling = div.nextElementSibling
            div.remove()
            const therapist = new VisitTherapist(bodyObj).render()
            if (prevSibling) {
                prevSibling.after(therapist)
            } else if (nextSibling) {
                nextSibling.before(therapist)
            } else {
                const container = document.getElementById('cards');
                container.append(therapist)
            }
        })

    }
}

