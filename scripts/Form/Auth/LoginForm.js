import Form from '../Form.js'
import {Input} from "../../components/Input.js"
import {InputEmailTag, InputPasswordTag, InputSubmitTag} from "../../State/TagsObjects/Login/VisitForm.js";
import {loginUser, getCards} from '../../functions/indexFunctionReimport.js'
import {VisitTherapist, VisitCardiologist, VisitDentist} from "../../Visited/indexVisitReimport.js"



export class LoginForm extends Form {
    renderForm(){
        const form = this.render()
        this.form = form;
        const inputEmail = new Input(InputEmailTag).render()
        const InputPassword = new Input(InputPasswordTag).render()
        const InputSubmit = new Input(InputSubmitTag).render()
        form.addEventListener('submit', this.submitHandler)
        form.append(inputEmail,InputPassword,InputSubmit)
        return form
    }

    submitHandler = (e) => {
        e.preventDefault()
        const login = this.form.querySelector('#register-email').value
        const password = this.form.querySelector('#register-password').value
        const data = {
            email: `${login}`,
            password: `${password}`
        }
        loginUser(data).then((response) => {
            const {token} = response.data
            if (token) {
                localStorage.setItem('token', token)
            }
        }).then(() => {
            const link = document.getElementById('nav-link');
            link.textContent = 'Создать карточку'
            getCards().then((res) => {
                res.data.forEach((item) => {
                    const {"select-doctor": doctor} = item
                    const container = document.getElementById('cards');
                    switch (doctor) {
                        case 'therapist':
                            const cardTherapist = new VisitTherapist(item).render()
                            container.append(cardTherapist)
                            break;
                        case 'cardiologist':
                            const cardCardiologist = new VisitCardiologist(item).render()
                            container.append(cardCardiologist)
                            break;
                        case 'dentist':
                            const cardDentist = new VisitDentist(item).render()
                            container.append(cardDentist)
                            break;
                    }
                })
            })
        })
    }
}

