export const noItems = {
    tag: 'h1',
    attr: {
        className: 'no-item-text',
        id: 'no-item'
    },
    content: 'Нет карточек!'
}
