export const TextArea = {
    tag: 'textArea',
    attr: {
        className: 'textarea-field',
        name: 'textarea',
        required: false,
        placeholder: 'Описание'
    }
}

export const Target = {
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'target',
        required: true,
        placeholder: 'Введите цель визита',
    },
    errorText: 'Поле обязательно для заполнения!'
}

export const FullName ={
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'full-name',
        required: true,
        placeholder: 'ФИО'
    },
    errorText: 'Поле обязательно для заполнения!'
}

export const Age = {
    tag: 'input',
    attr: {
        className: 'input-field',
        name: 'age',
        required: true,
        placeholder: 'Введите ваш возвраст'
    },
    errorText: 'Поле обязательно для заполнения!'
}

export const SelectPriority = {
    low: 'Низко',
    medium: 'Приоритетно',
    height: 'Срочно'
}

export const SelectPrioritySearch = {
    default: 'Выберите приоритет',
    low: 'Низко',
    medium: 'Приоритетно',
    height: 'Срочно'
}

export const ButtonCreate = {
    tag: 'input',
    attr: {
        className: 'input-btn',
        type: 'submit',
        value: 'Создать',
    }
}

export const SearchInput ={
    tag: 'input',
    attr: {
        className: 'search-input',
        type: 'text',
        id: 'search',
        placeholder: 'Поиск по ФИО'
    }
}
