export const LastDayVisited = {
    tag: 'input',
    attr: {
        type: 'text',
        className: 'input-field',
        name: 'last-day-visited',
        required: true,
        placeholder: 'Дата последнего визита: 01.01.2020'
    },
    errorText: 'Поле обязательно для заполнения!'
}
